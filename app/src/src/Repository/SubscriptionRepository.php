<?php

namespace App\Repository;

use App\Entity\Subscription;
use Doctrine\ORM\EntityRepository;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class SubscriptionRepository extends EntityRepository
{
}
